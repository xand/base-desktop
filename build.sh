#!/bin/bash

rm -rf dist
mkdir dist

node r.js -o build.js
node r.js -o css.js

mkdir dist/fonts
cp -R code/fonts/* dist/fonts 
