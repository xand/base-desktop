({
    baseUrl: "code",
    paths: {
        jquery: "js/lib/jquery",
	foundation: "js/lib/foundation",
	mustache: "js/lib/mustache",
	i18next: "js/lib/i18next",
	desktop: "js/lib/desktop",
	text: "js/lib/text"
    },
    name: "desktop",
    out: "dist/js/desktop.js"
})
