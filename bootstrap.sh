#!/usr/bin/env bash

# Create steps directory
if [ ! -d /var/vagrant-step ]; then
	sudo mkdir -p /var/vagrant-step
fi

# Common tasks
if [ ! -f /var/vagrant-step/1.lock ]; then
	sed -i 's/http:\/\/mirror\.rackspace\.com/http:\/\/archive\.ubuntu\.com/g' /etc/apt/sources.list
	apt-get update

	apt-get -y install build-essential
	apt-get -y install g++
	apt-get -y install python
	apt-get -y install python-software-properties
	apt-get -y install software-properties-common

	apt-get -y install python-pip python3-pip
	pip install grip

	export tz='Europe/Madrid'
	cp -vf /usr/share/zoneinfo/$tz /etc/localtime
	echo $tz | tee /etc/timezone
	
	touch /var/vagrant-step/1.lock
fi

# Install nginx
if [ ! -f /etc/nginx/nginx.conf ]; then
    echo "Installing nginx..."
    apt-get -y install nginx
    apt-get -y install yui-compressor
    
    update-rc.d nginx defaults

    cp /vagrant/nginx.conf /etc/nginx/sites-available/main
    ln -s /etc/nginx/sites-available/main /etc/nginx/sites-enabled/main

    service nginx restart
    
    echo "nginx installed."
fi

#Install nodejs
if [ ! -f /var/vagrant-step/2.lock ]; then
    apt-get -y install nodejs
    ln -s /usr/bin/nodejs /usr/bin/node
    
    apt-get -y install npm

    npm install -g requirejs
    
    touch /var/vagrant-step/2.lock
fi

# Configure grip as service
if [ ! -f /etc/init.d/grip ]; then
    mkdir /var/run/grip
    
    cp /vagrant/grip.sh /opt/grip.sh
    chmod 755 /opt/grip.sh

    cp /vagrant/grip.init.d /etc/init.d/grip
    chmod 755 /etc/init.d/grip
fi

# Last line to confirm that everything is OK
echo "VM provision done."
echo "Nginx is listening on localhost:8888"
echo "Grip is listening on localhost:6419, you need to start it before with 'sudo service grip start'"


