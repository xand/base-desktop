define(['jquery', 'mustache', 'i18next', 'text!desktop.html'], function($, m, i18n, desktopTemplate){
    var defaultSettings = {
        config: [],
        sidebar: '#sidebar',
        container: '#container',
        loadingSpinnerUrl: '/images/loading-spinner.gif',
        templatesBaseUrl: '/mst',
        defaultLocation: '/index.html',
        preloadTemplates: [
        ]
    };
    
    var desktop = function() {
        var self = {};
        self.settings = {};
        self.templates = {};
        self.hashFunctions = {};
        
        function getTemplate(templateName) {
            var output = self.templates[templateName] || '';
            return output;
        }
        
        function render(templateName, data, i18n, element) {
            var template = getTemplate(templateName);
            var rendered = m.render(template, data);
            
            if(i18n) {
                rendered = $(rendered).i18n();
            }
            
            if(element) {
                $(element).html(rendered);
            }
            
            return rendered;
        }
        
        function renderComposition(parentSlug, slug) {
            var compositionName = parentSlug + '-' + slug + '-composition';

            // find subnav configuration
            var subnavConfig;
            $.each(self.settings.config, function(i,v) {
                if(v.slug == parentSlug) {
                    subnavConfig = v.subnav;
                }
            });
                                
            setupSubnav(subnavConfig, parentSlug, slug);
            setupMainPane(compositionName);
        }
        
        function setupSubnav(subnavConfig, parentSlug, selectedSlug) {
            var data = {};
            data.title = subnavConfig.title;
            data.elements = [];
            
            $.each(subnavConfig.elements, function(i,v){
                data.elements.push({
                    slug: '#/' + parentSlug + '/' + v.slug,
                    caption: v.caption
                });
            });
            
            render('subnav', data, i18n, $(self.settings.container).find('.subnav-container'));
        }
        
        function setupMainPane(compositionName) {
            var compositionElement = $(self.settings.container).find('.composition-container');
            
            // cleanup old modules if any
            $(compositionElement).find('[data-module]').each(function(i,v){
                var module = $(v).data('desktop-module');
                if(module) {
                    module.destroy();
                }
            });
            
            render(compositionName, {}, i18n, compositionElement);

            $(compositionElement).find('[data-module]').each(function(i,v){
                $(v).html('<img src="' + self.settings.loadingSpinnerUrl + '">');
                var moduleName = $(v).data('module');
                moduleName = 'app/' + moduleName;
                require([moduleName], function(m){
                    try {
                        var mInstance = m();
                        mInstance.init(v, self);
                        $(v).data('desktop-module', mInstance);
                    } catch(e) {
                        console.error('Error rendering module with name ' + moduleName, e);
                    }
                });
            });
        }
        
        function markSelectedLinks() {
            var hash = window.location.hash;
            var splitted = hash.split('/');
            
            var parentSlug = splitted[1];
            var slug = splitted[2];
            
            // first the sidebar
            $(self.settings.sidebar).find('li').removeClass('active');
            $(self.settings.sidebar).find('a').each(function(i,v) {
                var href = $(v).attr('href');
                if(href.indexOf('#/' + parentSlug) == 0) {
                    $(v).parents('li').addClass('active');
                }
            });
            
            // then, the subnav
            $(self.settings.container).find('.sub-nav dd').removeClass('active');
            $(self.settings.container).find('.sub-nav a').each(function(i,v){
                var href = $(v).attr('href');
                if(href.indexOf('#/' + parentSlug + '/' + slug) == 0) {
                    $(v).parents('dd').addClass('active');
                }
            });
        }
        
        function monitorHashchange() {
            $(window).bind('hashchange', function(event){
                var newHash = location.hash;
                var fn = self.hashFunctions[newHash];
                if(fn) {
                    fn();
                } else {
                    var splitted = window.location.hash.split('/');
            
                    var parentSlug = splitted[1];
                    var slug = splitted[2];
                    
                    renderComposition(parentSlug, slug);
                    markSelectedLinks();
                }
            });
        }
        
        function setupRoutes() {
            $.each(self.settings.config, function(i,v){
                if(v.fn) {
                    var slug = '#/' + v.slug;
                    self.hashFunctions[slug] = function(event) {
                        v.fn();
                    };
                } else if(v.subnav && v.subnav.elements) {
                    $.each(v.subnav.elements, function(k,x) {
                        var slug = '#/' + v.slug + '/' + x.slug;
                        if(x.fn) {
                            self.hashFunctions[slug] = function(event) {
                                x.fn();
                            };
                        }
                    });
                }
            });
        }
        
        function setupSidebar() {
            var data = {};
            data.elements = [];
            
            $.each(self.settings.config, function(i,v){
                var slug = '';
                
                if(v.subnav && v.subnav.elements) {
                    slug = '#/' + v.slug + '/' + v.subnavDefault;
                } else {
                    slug = '#/' + v.slug;
                }
                
                data.elements.push({
                    slug: slug,
                    caption: v.caption
                });
            });
            
            render('sidebar', data, i18n, $(self.settings.sidebar));
        }
        
        function processDefault() {
            if(window.location.hash) {
                window.location.href = self.settings.defaultLocation;
            } else {
                var slug = '/';
                if(self.settings.elementDefault) {
                    slug = slug + self.settings.elementDefault + '/';
                    
                    $.each(self.settings.config, function(i,v){
                        if(v.slug == self.settings.elementDefault) {
                            slug = slug + v.subnavDefault + '/';
                            return;
                        }
                    });
                    window.location.hash = slug;
                }
            }
        }
    
        function parseTemplates(data) {
            $(data).find('[type="x-tmpl-mustache"]').each(function(k,y){
                var templateId = $(y).attr('id');
                var templateContent = $(y).html();
            
                self.templates[templateId] = templateContent;
            });
        }
    
        self.init = function(options) {
            self.settings = $.extend(defaultSettings, options || {});
            
            parseTemplates(desktopTemplate);
            
            $.each(self.settings.preloadTemplates, function(i, v) {
                $.get(self.settings.templatesBaseUrl + '/' + v, function(d){
                    parseTemplates(d);
                    
                    if(i == (self.settings.preloadTemplates.length - 1)) {
                        monitorHashchange();
                        setupRoutes();
                        setupSidebar();
                        processDefault();
                    }
                });
            });
        };
        
        self.confirm = function(title, content, callback) {
            $('#desktop-confirmation-dialog').remove();
            
            var rendered = render('desktop-confirmation-dialog', {content: content, title: title}, i18n);
            
            $(rendered).find('button[data-dialog-accept]').on('click', function(event){
                if(callback) {
                    callback();
                }
                $('#desktop-confirmation-dialog').foundation('reveal', 'close');
            });
            
            $(rendered).find('button[data-dialog-cancel]').on('click', function(event){
                $('#desktop-confirmation-dialog').foundation('reveal', 'close');
            });
            
            $('body').append(rendered);
            
            $(document).foundation('reveal', 'reflow');
            $('#desktop-confirmation-dialog').foundation('reveal', 'open');
        };
        
        self.alert = function(type, content) {
            render('desktop-alert', {content: content, type: type}, i18n, '#desktop-alert-placeholder');
            $(document).foundation('alert', 'reflow');
        };
        
        self.popup = function(content, showClose, size) {
            $('#desktop-popup').remove();
            
            if(typeof content == 'object') {
                var copy = content.clone();
                content = $('<div>').append(copy).html();
            }
                        
            if(!showClose) {
                showClose = false;
            }
            
            if(!size) {
                size = 'small'
            }
            
            var mData = {
                content: content,
                showClose: showClose,
                size: size
            };
            
            var rendered = render('desktop-popup-dialog', mData, i18n);
            $('body').append(rendered);
            
            $(document).foundation('reveal', 'reflow');
            $('#desktop-popup').foundation('reveal', 'open');
            $(document).foundation('abide', 'reflow');
            
            return $('#desktop-popup');
        };
        
        self.closePopup = function() {
            $('#desktop-popup').foundation('reveal', 'close');
        };
        
        self.infoPage = function(content) {
            var mData = {
                content: content
            };

            render('desktop-info-page', mData, i18n, 'body');
        };
        
        self.render = render;
        
        self.getTemplate = getTemplate;
        
        return self;
    };
    
    return desktop;
});


