requirejs.config({
    //By default load any module IDs from js/lib
    baseUrl: 'js/lib',
    //except, if the module ID starts with "app",
    //load it from the js/app directory. paths
    //config is relative to the baseUrl, and
    //never includes a ".js" extension since
    //the paths config could be for a directory.
    paths: {
        app: '../app',
        jquery: 'jquery'
    },
    shim: {
        'foundation': {
            deps: ['jquery'],
            exports: 'Foundation'
        }
    },
    // please see: http://stackoverflow.com/questions/8315088/prevent-requirejs-from-caching-required-scripts
    urlArgs: "v=" + (new Date()).getTime()
});

define([
            'jquery', 
            'foundation', 
            'mustache', 
            'i18next', 
            'desktop'
    ], function($, f, m, i18n, desktop){
    
        i18n.init({ lngWhitelist: ['dev'], postProcess: 'sprintf' }, function(err, t){
            var mainDesktopConfig = {
                sidebar: '#sidebar',
                container: '#container',
                elementDefault: 'system',
                loadingSpinnerUrl: '/images/loading-spinner.gif',
                templatesBaseUrl: '/mst',
                preloadTemplates: [
                    'system-users-composition.html',
                    'clock-world-clock-composition.html',
                    'users-grid.html',
                    'clock.html'
                ],
                config: [
                    {
                        slug: 'system',
                        caption: i18n.t('menu.system.caption'),
                        subnavDefault: 'users',
                        subnav: {
                            title: i18n.t('menu.system.subnav.title'),
                            elements: [
                                {
                                    slug: 'users',
                                    caption: i18n.t('menu.system.subnav.users.caption')
                                },
                                {
                                    slug: 'click-me',
                                    caption: i18n.t('menu.system.subnav.clickMe.caption'),
                                    fn: function() {
                                        var msg = i18n.t('generic.message.clickedOnLink', {sprintf: ['Click me!']});
                                        alert(msg);
                                    }
                                }
                            ]
                        }
                    },
                    {
                        slug: 'clock',
                        caption: i18n.t('menu.clock.caption'),
                        subnavDefault: 'world-clock',
                        subnav: {
                            title: i18n.t('menu.clock.subnav.title'),
                            elements: [
                                {
                                    slug: 'world-clock',
                                    caption: i18n.t('menu.clock.subnav.world-clock.caption')
                                }
                            ]
                        }
                    },
                    {
                        slug: 'click-me',
                        caption: i18n.t('menu.click-me.caption'),
                        fn: function() {
                            var msg = i18n.t('generic.message.clickedOnLink', {sprintf: ['Click me']});
                            alert(msg);
                        }
                    }
                ]
            };
        
            var mainDesktop = desktop();
            mainDesktop.init(mainDesktopConfig);
        });
    });


