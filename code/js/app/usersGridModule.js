define(['jquery', 'i18next'], function($, i18n){
    var gridTemplateName = 'usersGrid';
    
    var testUsersData = [
        {
            name: 'Robert',
            surname: 'White',
            email: 'r.white@domain.com',
            phone: '+34 636 589 656'
        },
        {
            name: 'Jason',
            surname: 'Jones',
            email: 'j.jones@domain.com',
            phone: '+34 636 589 667'
        },
        {
            name: 'Barbara',
            surname: 'Wright',
            email: 'b.wright@domain.com',
            phone: '+34 636 589 668'
        },
        {
            name: 'Daniel',
            surname: 'Green',
            email: 'd.green@domain.com',
            phone: '+34 636 589 669'
        },
        {
            name: 'Kimberly',
            surname: 'Campbell',
            email: 'k.campbell@domain.com',
            phone: '+34 636 589 670'
        }
    ];
    
    var usersGridModule = function() {
        var self = {};
        
        function bindDeleteButton() {
            $(self.moduleElement).find('[data-btn-delete]').on('click', function(event){
                event.preventDefault();
                
                var name = $(event.target).data('name');
                var surname = $(event.target).data('surname');
                
                var confirmTitle = i18n.t('usersModule.message.delete.title');
                var confirmMsg = i18n.t('usersModule.message.delete.content', {sprintf: [name, surname]});
                self.desktop.confirm(confirmTitle, confirmMsg, function(){
                    var alertMsg = i18n.t('usersModule.message.delete.userDeleted', {sprintf: [name, surname]});
                    self.desktop.alert('alert', alertMsg);
                });
            });
        }
        
        self.init = function(element, desktop) {
            self.moduleElement = element;
            self.desktop = desktop;
            
            var data = {};
            data.users = testUsersData;
            
            self.desktop.render(gridTemplateName, data, i18n, self.moduleElement);
            
            bindDeleteButton();
        };
        
        self.destroy = function() {
            $(self.moduleElement).off();
            $(self.moduleElement).html('');
        };
        
        return self;
    };
    
    return usersGridModule;
});
