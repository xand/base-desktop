define(['jquery', 'i18next'], function($, i18n){
    var btnTemplateName = 'usersAddUserBtn';
    var formTemplateName = 'usersAddUserForm'
    
    var usersAddUserModule = function() {
        var self = {};
        
        function bindMainButton() {
            $(self.moduleElement).find('[data-btn-add-user]').on('click', function(event){
                event.preventDefault();
                
                var rendered = self.desktop.render(formTemplateName, {});
                var popup = self.desktop.popup(rendered, true);
                bindFormSubmit(popup);
                bindFormCancelButton();
            });
        }

        function bindFormSubmit(popup) {
            var form = $(popup).find('form');
            
            $('#desktop-popup').find('form')
                .on('invalid.fndtn.abide', function(){

                }).on('valid.fndtn.abide', function(){
                    var name = $(form).find('input[name="name"]').val();
                    var surname = $(form).find('input[name="surname"]').val();
                    var email = $(form).find('input[name="email"]').val();
                    var phone = $(form).find('input[name="phone"]').val();
                    
                    var msg = i18n.t('usersModule.message.save.userSaved', {sprintf: [name, surname, phone, email]});
                    self.desktop.alert('success', msg);
                    self.desktop.closePopup();
                });
        }

        function bindFormCancelButton() {
            $('#desktop-popup').find('[data-btn-new-user-form-cancel]').on('click', function(event){
                event.preventDefault();
                self.desktop.closePopup();
            });
        }
        
        self.init = function(element, desktop) {
            self.moduleElement = element;
            self.desktop = desktop;
            
            self.desktop.render(btnTemplateName, {}, i18n, self.moduleElement);
            
            bindMainButton();
        };
        
        self.destroy = function() {
            $(self.moduleElement).off();
            $(self.moduleElement).html('');
        };
        
        return self;
    };
    
    return usersAddUserModule;
});
