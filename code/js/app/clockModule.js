define(['jquery', 'i18next'], function($, i18n){
    var templateName = 'clock';

    var usersModule = function() {
        var self = {};

        function showTime() {
            var d = new Date();

            var myTimeMs = d.getTime();
            var myOffsetMs = d.getTimezoneOffset() * 60 * 1000;
            var utcTimeMs = myTimeMs + myOffsetMs;
            
            var requiredOffset = $(self.moduleElement).data('utc-offset');
            var offsetMs = requiredOffset * 60 * 60 * 1000;

            var requiredTimeMs = utcTimeMs + offsetMs;
            
            var rDate = new Date(requiredTimeMs);
            
            var data = {};
            data.hours = rDate.getHours();
            if(data.hours < 10) {
                data.hours = '0' + data.hours;
            }
            
            data.minutes = rDate.getMinutes();
            if(data.minutes < 10) {
                data.minutes = '0' + data.minutes;
            }
            
            data.seconds = rDate.getSeconds();
            if(data.seconds < 10) {
                data.seconds = '0' + data.seconds;
            }
            
            data.day = rDate.getDate();
            if(data.day < 10) {
                data.day = '0' + data.day;
            }
            
            data.month = rDate.getMonth() + 1;
            if(data.month < 10) {
                data.month = '0' + data.month;
            }
            
            data.year = rDate.getFullYear();
            
            if(requiredOffset >= 0) {
                data.offset = '+' + requiredOffset;
            } else {
                data.offset = requiredOffset;
            }
            
            self.desktop.render(templateName, data, i18n, self.moduleElement);

            setTimeout(function(){showTime();}, 1000);
        }

        self.init = function(element, desktop) {
            self.moduleElement = element;
            self.desktop = desktop;

            showTime();
        };
        
        self.destroy = function() {
            $(self.moduleElement).off();
            $(self.moduleElement).html('');
        };

        return self;
    };

    return usersModule;
});
