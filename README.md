# Base Desktop

1. [Introducción](#user-content-a1-introduccion)
2. [Dependencias](#user-content-a1-dependencias) 
3. [Escritorio](#user-content-a1-escritorio)
4. [Máquina Vagrant](#user-content-a1-vagrant)
5. [Bootstrap del proyecto con Base Desktop](#user-content-a1-bootstrap)
6. [Desarrollo de módulos](#user-content-a1-desarrollo-modulos)  

## <a name='a1-introduccion'></a>Introducción
El proyecto trata de desarrollar una interfaz (GUI) para las aplicaciones WEB. El proyecto consiste en un escritorio basado en Javascript que permite la inclusión de módulos propios de la aplicación ofreciendo las funcionalidades comunes (gestión de plantillas, menús, alerts, popups, etc).

La motivación para el desarrollo de esta herramienta es simplemente la creencia en que el servidor no tiene que realizar las tareas del GUI, es decir, sólo tiene que servir para ofrecer información y ejecutar comandos, quedando todas las tareas de generación del GUI para el cliente.  

Concretamente se persiguen los siguientes objetivos:

1. Poder desplegar un GUI de forma rápida evitando el tener que reimplementar las funcionalidades comúnes.
2. *Modularización de componentes*. Cada módulo funcional cuenta con su plantilla (mst) y su implementación lógica (js).
3. Una apariencia estética moderna.
4. Una abstracción completa de la tecnología del servidor. Es decir, se parte de la idea que el propio servidor no realiza ninguna generación de páginas WEB. De hecho, las únicas funcionalidades ofrecidas por el servidor, para que el escritorio funcione, es la capacidad de servir ficheros (cargas iniciales) y responder a las peticiones HTTP oportunas.
5. *Download once*. Cuando el usuario accede por primera vez a la página con el escritorio este intentará descargar todos los ficheros necesarios para posteriormente lanzar, únicamente, las peticiones HTTP funcionales.
6. Minificación de los ficheros a descargar. El escritorio se ha implementado teniendo en mente la futura minificación de los ficheros que intervienen.
7. *Cross browser*. Debido a que se utilizan las librerías de tercero compatibles con todos los navegadores modernos, supuestamente, no tiene que haber ningún problema en renderizar el escritorio en ellos.
8. *Mobile First*. Debido a la utilización de Foundation, el escritorio es Mobile First, siempre y cuando se respeten las normas de la herramienta a la hora de desarrollar los módulos para conseguir tal fin. 

## <a name='a1-dependencias'></a>Dependencias
El escritorio depende de los siguientes componentes externos:

* [RequireJS](http://requirejs.org/). Gestión de las dependencias JavaScript.
* [Foundation](http://foundation.zurb.com/). Librería de componentes gráficos, grid, etc.
* [Modernizr](http://modernizr.com/). Detección de navegadores antiguos.
* [Jquery](https://jquery.com/). Librería jQuery.
* [Mustache](https://github.com/janl/mustache.js). Motór de plantillas Mustache.
* [Jquery-Routes](https://github.com/thorsteinsson/jquery-routes). Implementación de rutas con hash.
* [I18Next](http://i18next.com/). Librería para la internacionalización (I18N). Esta librería es opcional.

## <a name='a1-escritorio'></a>Escritorio
Al igual que todos los módulos, el escritorio se define como un módulo más. La forma de definir los módulos se explica con todo detalle en el sitio web de [RequireJS](http://requirejs.org/).

Hay que recalcar que, tanto el módulo desktop, como todos los demás módulos funcionales se definen como [funciones](http://requirejs.org/docs/api.html#funcmodule) permitiendo de esta forma reutilizar varios módulos en una misma aplicación. Por ejemplo, es posible definir más de un escritorio, un caso práctico, por ejemplo, sería la definición de dos escritorios: uno para login y otro el principal.

El módulo escritorio se define en el fichero desktop.js y tiene una serie de métodos.

### Métodos

* *infoPage(content)*. Reemplaza completamente el contenido de la ventana con el contenido del parámetro *content*. Es una función útil, por ejemplo, cuando se quiere forzar al cliente recargar la página mostrando un mensaje.
* *popup(content, showClose, size)*. Muestra una ventama emergente con el contenido HTML (*content*) aleatorio. Los demás parámetros son: *showClose* - indica si se tiene que mostrar el aspa para cerrar la ventana, los posibles valores para este parámetro es *true* o *false*; *size* - tamaño de la ventana emergente. El tamaño estará controlado por [Foundation](http://foundation.zurb.com/docs/components/reveal.html) y podrá tomar uno de los siguientes valores: *tiny, small, medium, large, xlarge, full*. 
* *alert(type, content)*. Muestra una alerta al usuario en la esquina inferior derecha del escritorio. **No se trata de una ventana modal**, más bien es un aviso informativo. El parámetro *type* puede tomar uno de los siguientes [valores](http://foundation.zurb.com/docs/components/alert_boxes.html): *success, warning, info, alert, secondary*. Estos valores son los definidos por Foundation. El parámetro *content* es el texto que se le mostrará al usuario, no debe de llevar código HTML.  
* *confirm(title, content, callback)*. Muestra una ventana de confirmación estilizada con Foundation. Su equivalente en Javascript sería la función confirm. Los parámetros son el título (*title*) y el contenido HTML (*content*) que se le mostrarán al usuario. El *callback* es una función que será llamada en el caso de que el usuario pulse 'Aceptar'. En el caso de que el usuario pulse 'Cancelar' simplemente se cierra la ventana, similar a la función confirm nativa de Javascript. 
* *getTemplate(templateName)*. Devuelve una plantilla guardada previamente. El nombre de la plantilla (*templateName*) tiene que coincidir con el nombre en el id de la declaración de esta.

### Estructura

Generalmente el desarrollador es libre de eligir la estructura de la página, siempre y cuando respete las nomenclaturas de *id* y *class* de los componentes clave. No obstante, aquí está la estructura original con la que se ha desarrolado la herramienta:

```html
	<!doctype html>
	<html>
		<head>
			<title>Title :: Desktop</title>
			
			<link rel="stylesheet" type="text/css" href="css/normalize.css">
			<link rel="stylesheet" type="text/css" href="css/foundation.min.css">
			<link href="css/foundation-icons.css" rel="stylesheet">
			<link href="css/font-awesome.css" rel="stylesheet">
			<link rel="stylesheet" type="text/css" href="css/desktop.css">
	
			<script data-main='js/main' src='js/lib/require.js'></script>
			
			<script src="js/lib/modernizr.js"></script>
		</head>
		<body>
			<header></header>    
	        
	        <div id="sidebar"></div>
	
			<div id="container">
				<div class='subnav-container'></div>
				<div class='composition-container'></div>
			</div>
	                
			<div id='desktop-alert-placeholder'></div>
		</body>
	</html>
```

### Comportamiento

El funcionamiento del escritorio es simple y se puede resumir en los siguientes pasos:

1. [Inicialización](#user-content-inicializacion).
2. [Descarga de los ficheros necesarios](#user-content-descarga-ficheros).
3. [Estructura de plantillas](#user-content-estructura-plantillas).
3. [Renderizado de la composición solicitada](#user-content-renderizado-composicion).
4. [Renderizado de los módulos definidos en la composición](#user-content-renderizado-modulos).

### <a name="inicializacion"></a>Inicialización

La inicialización del escritorio se realiza en el fichero main.js, aunque el desarrollador es libre de cambiarle el nombre a dicho fichero siempre y cuando se realicen todos los cambios que proceden.

La inicialización de RequireJS, teniendo en cuenta que se mantiene la estructura original, sería la siguiente:

```javascript
	requirejs.config({
	    baseUrl: 'js/lib', // directorio de los módulos por defecto
	    paths: {
	        app: '../app', // aquellos módulos que empiecen por apps se cargarán de aquí
	        jquery: 'jquery' // definición de jquery
	    },
	    shim: {
	        'foundation': {
	            deps: ['jquery'],
	            exports: 'Foundation'
	        }
	    },
	    urlArgs: "v=" + (new Date()).getTime() // con esto prevenimos la cache
	});
```

A su vez, la inicialización del escritorio es la siguiente:

```javascript
	define(['jquery', 'foundation', 'mustache', 'i18next', 'desktop'], function($, f, m, i18n, desktop){
		i18n.init({ lngWhitelist: ['dev'], postProcess: 'sprintf' }, function(err, t){
	        var mainDesktopConfig = {...};
	    
	        var mainDesktop = desktop();
	        mainDesktop.init(mainDesktopConfig);
	    });
	});
```


#### I18N

El motor de la internacionalización se inicializa de la siguiente manera: 

```javascript
	i18n.init({ lngWhitelist: ['dev'], postProcess: 'sprintf' });
```

Queda fuera del ámbito de este documento la descripción de las opciones y funcionalidades que ofrece la librería i18next. Para ello puede visitar su página [web](http://i18next.com/). Sólo cabe destacar que con la configuración por defecto el fichero de literales se descargará de la siguiente dirección: */locales/dev/translation.json*.

Este fichero deberá de tener el siguiente formato:

```javascript
	{
	    "testModule": {
	        "label": {
	            "test": "My first label"
	        }
	    }
	}
```	

#### Desktop

La inicialización del propio escritorio se realiza de la siguiente manera:

```javascript
	var config = {...}; // configuración
	var mainDesktop = desktop(); // se instancia el objeto
	mainDesktop.init(config); // se configura el objeto y se renderiza
```	

El objeto *config* es un objeto JSON que define la configuración global del escritorio, la configuración de menús y la composiciones correspondientes.

Se puede tomar como ejemplo el siguiente objeto con la descripción de cada una de las opciones:

```javascript
	{
        sidebar: '#sidebar', // selector para el contenedor de sidebar, por defecto: '#sidebar'
		container: '#container', // selector para el contenedor del submenú y las composiciones, por defecto: '#container'
		elementDefault: 'system', // opción del menú seleccionada por defecto, nada más cargar la página
        templatesListUrl: '/templates/list', // url desde la que se descargarán las plantillas mst
        loadingSpinnerUrl: '/images/loading-spinner.gif', // url de la imagen spinner
		templatesBaseUrl: '/mst', // ruta desde la que se cargarán las plantillas
		preloadTemplates: [ // plantillas precargadas
	        'system-users-composition.mst',
	        'clock-world-clock-composition.mst',
	        'users-grid.mst',
	        'clock.mst'
		],
		config: [ // configuración de los menús, submenús y composiciones
			{
				slug: 't1', // slug para la opción del menú
				caption: 'Administration', // etiqueta para la opción del menú
				subnavDefault: 't11', // opción del submenú seleccionada por defecto
                subnav: { // definición del submenú
					title: 'Navigation', // título del submenú
					elements: [ // elementos del submenú
						{
							slug: 't11', // slug de la opción del submenú
							caption: 'T11' // etiqueta de la opción del submenú
						},
						{
							slug: 't12',
							caption: 'T12'
						},
						{
							slug: 't13',
							caption: 'T13',
							fn: function() { // función a ejecutar cuando el usuario pulsa sobre la opción del submenú
								alert('T13 clicked');
							}
						}
					]
				}
			},
			{
				slug: 'reset',
				caption: 'Reset',
				fn: function() {// función a ejecutar cuando el usuario pulsa sobre la opción del submenú
					alert('Reset clicked');
				}
			}
		]
	}
```

Hay que resaltar que tanto en el caso de una opción del menú como en el de una opción del submenú, si existe una función a ejecutar el escritorio la ejecutará sin más, es decir, no hará ningún procesamiento más.

### <a name="descarga-ficheros"></a>Descarga de los ficheros necesarios

El escritorio intentará descargar todas las plantillas de todos los componentes nada más inicializarse. Los ficheros están definidos en la opción de configuración *preloadTemplates*. Se intenta realizar una descarga masiva la primera vez que el usuario abre la página y después simplemente se utilizan las plantillas descargadas.

Posteriormente se puede recuperar la plantilla recucurriendo a la siguiente sentencia:

```javascript
	desktop.getTemplate('identificador_plantilla');
```

Si el escritorio no puede encontrar la plantilla solicitada devolverá una cadena vacía.

### <a name="estructura-plantillas"></a>Estructura de plantillas

Una plantilla del escritorio tiene el siguiente formato:

```html
	<div>
	    <script id="identificador_plantilla" type="x-tmpl-mustache">
	        <!-- Contenido plantilla -->
	    </script>
	</div>
```	

Esta estructura está pensada para los propósitos de inclusión de todas las plantillas en un sólo fichero y posterior minificación y compresión del mismo. Es decir, el fichero resultante debería de tener el siguiente formato:

```html
	<div>
	    <script id="identificador_plantilla_1" type="x-tmpl-mustache">
	        <!-- Contenido plantilla 1 -->
	    </script>
	    <script id="identificador_plantilla_2" type="x-tmpl-mustache">
	        <!-- Contenido plantilla 2 -->
	    </script>
	    <script id="identificador_plantilla_3" type="x-tmpl-mustache">
	        <!-- Contenido plantilla 3 -->
	    </script>
	</div>
```	

### <a name="renderizado-composicion"></a>Renderizado de la composición solicitada

Una composición es una colección de elementos. Cada composición, a su vez, se define como una plantilla.

Es importante que el identificador de la composición tenga una estructura definida de la siguiente manera:

```
	<slugOpciónMenu>-<slugOpciónSubmenu>-composition
```	

Dentro de la plantilla puede estar ubicado cualquier tipo de layout, pero tiene que haber uno o varios elementos *div* con el atributo *data-plugin*. Este atributo determinará el plugin que se renderizará en el elemento.

Por ejemplo, si tenemos un módulo llamado *ClockModule* (definido en el js) podríamos renderizarlo definiendo un elemento *div* de estas características:

```html
	<div class='small-8 columns' data-plugin='clockModule'>
```	

En el momento de empezar el renderizado de un módulo en el elemento correspondiente se muestra un spinner 'Cargando'.

El módulo en sí quedará guardado como un atributo data del elemento en el que ha sido renderizado. Es decir, si posteriormente se quiere acceder al módulo se puede hacer de la siguiente manera:

```javascript
	$(element).data('desktop-module');
```	

## <a name='a1-vagrant'></a>Máquina Vagrant

Para el desarrollo del proyecto se utiliza una máquina Vagrant. La máquina Vagrant tiene los siguientes servicios instalados:

| Servicio | Puerto | Observaciones |
-----------|--------|---------------|
| nginx | 8888 | El servidor sirve para comprobar el funcionamiento de la aplicación |
| grip | 6419 | Servidor grip que sirve para visualizar los ficheros md. Este servidor se tiene que arrancar con el comando *sudo service grip start* |

## <a name='a1-bootstrap'></a>Bootstrap del proyecto con Base Desktop

Para iniciar un nuevo proyecto con Base Desktop se tiene que ejecutar el script script.sh.

## <a name='a1-desarrollo-modulos'></a>Desarrollo de módulos

El desarrollo de módulos funcionales se realiza en Javascript. Cada módulo tiene que tener una estructura determinada ya será instanciado por el objeto escritorio.

### Estructura general

Cada uno de los módulos desarrollados debe de estar guardado en el fichero js separado. Es decir, un fichero por módulo. El nombre del fichero debe de ser el siguiente: *nombreModulo.module.plugin.js*. Por ejemplo: *clock.module.plugin.js*.

La estructura general de los módulos es la siguiente:

```javascript
	define(['jquery', 'i18next'], function($, i18n){
	    var templateName = 'clock';
	
	    var clockModule = function() {
	        var self = {};
	
	        function privateMethod() {
	            return false;
	        }
	
	        self.init = function(element, desktop) {
	            self.moduleElement = element;
	            self.desktop = desktop;
	
	            privateMethod();
	        };
	        
	        self.destroy = function() {
	            $(self.moduleElement).off();
	            $(self.moduleElement).html('');
	        };
	
	        return self;
	    };
	
	    return clockModule;
	});
```	

Consideraciones a tener en cuenta:

* Todo módulo es una función Javascript que no acepta parámetros. Así es como el módulo será instanciado por el escritorio.
* La función mencionada debe de devolver el objeto *self* que de cara al exterior será el módulo instanciado que ofrece los métodos públicos correspondientes.
* El módulo debe de definir dos métodos públicos obligatorios:
	* *init(element, desktop)* -- será llamado en el momento de la inicialización pasando como parámetro el elemento HTML donde está ubicado y el desktop del que depende.
	* *destroy()* -- será llamado en el momento de destruir el módulo. No acepta ningún parámetro. Se tiene que encargar de realizar las tareas de limpieza.
* El nombre del fichero que contiene el módulo es importante, ya que de esta forma el mecanismo de RequireJS sabrá ubicarlo. Si el módulo se denomina *clockModule* el nombre del fichero contenedor debe de ser *clockModule.js*.

### Comunicación entre los módulos

Generamente los módulos serán estructuras atómicas. No obstante, siguiendo el principio DRY podrán exponer una serie de métodos públicos de la interfaz.

Se puede realizar la comunicación entre los distintos módulos de dos formas distintas:
1. Disparando los eventos jquery y escuchándolos de forma asíncrona.
2. Directamente llamando al método de módulo de forma síncrona. En este caso se podría obtener el objeto del módulo de la siguiente manera:

```html
	$('div[data-plugin="clock"]').data('desktop-module');
```	

### Parámetros de inicialización de los módulos

A veces, se require pasarle al módulo una serie de parámetros de inicialización. Para ello se pueden aprovechar los atributos *data* del HTML que contiene la composición.

Un ejemplo podría ser el *clockModule*. Primeramente establecemos en la composición el parámetro *utc-offset*:

```html
	<div class='small-4 columns' data-module='clockModule' data-utc-offset='-4'>
```	

Una vez instanciado el módulo lo leemos:

```javascript
	var requiredOffset = $(self.moduleElement).data('utc-offset');
```	





